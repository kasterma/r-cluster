# On Rserver

Edit riemann-<version>/etc/riemann.config to have host to 0.0.0.0

bin/riemann etc/riemann.config

edit config.rb to have

   set :bind, "0.0.0.0"

riemann-dash config.rb

riemann-health --host localhost --event-host rserver

The dashboard is now available on the rserver host on port 4567.
Note: On the top right there is a text field where localhost needs
to be replaced by the public name of the rserver host.

# On Rnode

Can only ssh to the rnode from the rserver and using user rserver.

riemann-health --host <from output of template> --event-host rnode<i>