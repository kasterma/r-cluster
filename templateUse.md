# How to use the template

1. Log into AWS comsole.
2. Go to cloudformation service.
3. Click button "Create stack".
4. Choose a name (should be different from the names of already running stacks).
5. Choose upload a template file, and pick the template file provided.
6. For the allowed ip put in your own public ip/32.  This is the address from which you can reach the r-server in the cluster.  Since the login for this account is published it is important to restrict access.  (use e.g. http://whatismyip.org/ to find your public ip)
7. Choose the instance type you want.
8. Choose a keyname.  This is the name of an ssh key you can use to login to the instance.  This is mostly for administration of the cluster.
9. No need to add tags.
10. Continue.  Now the stack will be created.  At this point the interface will show CREATE_IN_PROGRESS.  Refresh a couple of times until CREATE_COMPLETE is shown.  Now your cluster is running and ready for use.
11.  While having the just launched stack selected, choose Outputs.  There is one value shown RServerHost that is the public hostname of the r-server.  Go to port 8787 on that host with your webbrowser to log into the r-server.
12. Login is rserver with password rserver.
13. The urls for the nodes can be obtained using read.table("/tmp/nodes.txt").

    library(snow)

    ns <- read.table("/tmp/nodes.txt",stringsAsFactors=FALSE)

    cl  <- makeCluster(ns[1:2,], port=11446, type="SOCK")

    Note that the 2 in the last command is the number of nodes; this is
    just to rework ns to the form makeCluster expects.