sudo apt-get update
sudo apt-get install -y openjdk-6-jdk make emacs23 unzip python-setuptools

sudo apt-get install -y r-base

# mpi
sudo apt-get install -y r-cran-rmpi
sudo apt-get install -y libopenmpi-dev

Rscript -e "install.packages(c(\"devtools\", \"testthat\", \"snow\", \"rmpi\"), repos=\"http://cran.rstudio.com\")"

# riemann
sudo apt-get install -y ruby1.9.3
sudo gem install riemann-tools
