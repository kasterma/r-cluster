sudo cp /vagrant/vagrant/resources/id_rsa_rcluster /home/vagrant/.ssh/id_rsa
sudo cp /vagrant/vagrant/resources/id_rsa_rcluster.pub /home/vagrant/.ssh/id_rsa.pub
sudo cat /vagrant/vagrant/resources/id_rsa_rcluster.pub >> /home/vagrant/.ssh/authorized_keys
sudo cp /vagrant/vagrant/resources/ssh-config  /home/vagrant/.ssh/config
sudo chown vagrant:vagrant /home/vagrant/.ssh/*
