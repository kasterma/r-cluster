sudo apt-get update
sudo apt-get install -y openjdk-6-jdk make emacs23 unzip python-setuptools

sudo apt-get install -y r-base

sudo apt-get install -y gdebi-core
sudo apt-get install -y libapparmor1  # Required only for Ubuntu, not Debian
wget http://download2.rstudio.org/rstudio-server-0.97.551-i386.deb
sudo gdebi -n rstudio-server-0.97.551-i386.deb

# mpi
sudo apt-get install -y r-cran-rmpi
sudo apt-get install -y libopenmpi-dev

Rscript -e "install.packages(c(\"devtools\", \"testthat\", \"snow\", \"rmpi\"), repos=\"http://cran.rstudio.com\")"

# riemann

sudo apt-get install -y ruby1.9.3
wget http://aphyr.com/riemann/riemann-0.2.2.tar.bz2
tar xvfj riemann-0.2.2.tar.bz2
sudo gem install riemann-client riemann-tools riemann-dash
