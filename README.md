# r-cluster

Setting up a cluster of nodes to use with R through snow.

Make a clone of this repo (I made it in ~/projects/).  Then in R
execute the following commands to generate the template.  Here
<number> should be the number of nodes you want, and filename.template
the name you want to give to the template.

    setwd('~/projects/r-cluster/cloudformation/')
    source('~/projects/r-cluster/cloudformation/generateTemplate.R')
    writeLines(get.template(<number>), "filename.teplate")

After this you have a template in filename.template.  Now follow
the instructions in templateUse.md

If you want to use Riemann to see how the nodes are loaded see
riemann.md for notes on this.