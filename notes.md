# r-cluster

Setting up a cluster of nodes to use with R through snow.

open browser on localhost:8787 to get access to the rserver.  On this
server run

    > cl <- makeSOCKcluster(c("rnode1", "rnode2"), manual=TRUE)
    Manually start worker on rnode1 with
     /usr/lib/R/bin/Rscript /usr/local/lib/R/site-library/snow/RSOCKnode.R MASTER=precise32 PORT=11861 OUT=/dev/null SNOWLIB=/usr/local/lib/R/site-library
    Manually start worker on rnode2 with
     /usr/lib/R/bin/Rscript /usr/local/lib/R/site-library/snow/RSOCKnode.R MASTER=precise32 PORT=11861 OUT=/dev/null SNOWLIB=/usr/local/lib/R/site-library

Note that the commands to run on the nodes are not entirely correct.
The /etc/hosts file makes the server known as rserver (not precise32,
which is the virtualbox name).  For copy paste ease, here is the
command to run on the nodes.

     /usr/lib/R/bin/Rscript /usr/local/lib/R/site-library/snow/RSOCKnode.R MASTER=rserver PORT=11861 OUT=/dev/null SNOWLIB=/usr/local/lib/R/site-library


Then in the rserver can run

    > clusterCall(cl, function(x=2){x+1}, 5)
    [[1]]
    [1] 6

    [[2]]
    [1] 6

    > stopCluster(cl)

Due to older R version, mpi is not going to work on these nodes.

## Not manual

    makeSOCKcluster(c("rserver","rnode1","rnode2"), master="rserver")

## Riemann

On rserver

    cd riemann-<version>
    bin/riemann etc/riemann.conf

    riemann-health --host rserver --event-host rserver

On rnodes

    riemann-health --host rserver --event-host rnode<i>

## Amazon access credentials

set-aws-env.sh should contain

    export AWS_ACCESS_KEY=your-aws-access-key-id
    export AWS_SECRET_KEY=your-aws-secret-key

with your credentials filled in.

## setting up on AWS

First install and download everything on a base image (follow the
Vagrantfile for this, but note that the rstudio downloaded there is
for 32 bit systems; get an error about psmisc if you try to install
this on a 64 bit system).

Put keys in place.

    cl <- makeCluster("ubuntu@ec2-54-220-168-129.eu-west-1.compute.amazonaws.com", master="ec2-54-220-126-87.eu-west-1.compute.amazonaws.com", type="SOCK", port=11446)

Note: if you don't pass the port a random one is picked.  But then
likely not part of range that has been configured in the security
group.


    server.host <- system("hostname -f", intern=TRUE)

    clusterCall(cl, function(host) system(paste("riemann-health --host ", host)), server.host)